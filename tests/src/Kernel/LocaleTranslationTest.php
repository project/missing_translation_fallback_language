<?php

namespace Drupal\Tests\missing_translation_fallback_language\Kernel;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the fallback functionalities.
 */
class LocaleTranslationTest extends KernelTestBase {

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'system',
    'locale',
    'language',
    'missing_translation_fallback_language',
  ];

  /**
   * Fallback.
   *
   * @var \Drupal\language\ConfigurableLanguageInterface
   */
  protected $fallback;

  /**
   * Dutch.
   *
   * @var \Drupal\language\ConfigurableLanguageInterface
   */
  protected $dutch;

  /**
   * French.
   *
   * @var \Drupal\language\ConfigurableLanguageInterface
   */
  protected $french;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('locale', [
      'locales_location',
      'locales_source',
      'locales_target',
    ]);

    $this->fallback = ConfigurableLanguage::createFromLangcode('fb');
    $this->fallback->save();

    $this->dutch = ConfigurableLanguage::createFromLangcode('nl');
    $this->dutch->save();

    $this->french = ConfigurableLanguage::createFromLangcode('fr');
    $this->french->save();

    /**
     * @var \Drupal\locale\StringDatabaseStorage $locale_storage
     */
    $locale_storage = $this->container->get('locale.storage');
    $source = $locale_storage->createString([
      'source' => 'Source',
      'context' => '',
      'version' => \Drupal::VERSION,
    ])->save();
    $locale_storage->createTranslation([
      'lid' => $source->lid,
      'language' => $this->dutch->id(),
      'translation' => 'Target nl',
    ])->save();
    $locale_storage->createTranslation([
      'lid' => $source->lid,
      'language' => $this->fallback->id(),
      'translation' => 'Target fb',
    ])->save();

    $source = $locale_storage->createString([
      'source' => 'Source 2',
      'context' => '',
      'version' => \Drupal::VERSION,
    ])->save();
    $locale_storage->createTranslation([
      'lid' => $source->lid,
      'language' => $this->fallback->id(),
      'translation' => 'Target 2 fb',
    ])->save();
  }

  /**
   * Test the default translation workings.
   */
  public function testFallbackNotConfigured(): void {
    $this->assertEquals('Target nl', new TranslatableMarkup('Source', [], ['langcode' => $this->dutch->id()]));
    $this->assertEquals('Source 2', new TranslatableMarkup('Source 2', [], ['langcode' => $this->dutch->id()]));
    $this->assertEquals('Target fb', new TranslatableMarkup('Source', [], ['langcode' => $this->fallback->id()]));
    $this->assertEquals('Source', new TranslatableMarkup('Source', [], ['langcode' => $this->french->id()]));
  }

  /**
   * Test with a fallback set.
   */
  public function testFallbackConfigured(): void {
    $this->dutch->setThirdPartySetting('missing_translation_fallback_language', 'langcode', $this->fallback->id());
    $this->dutch->save();

    $this->assertEquals('Target nl', new TranslatableMarkup('Source', [], ['langcode' => $this->dutch->id()]));
    $this->assertEquals('Target 2 fb', new TranslatableMarkup('Source 2', [], ['langcode' => $this->dutch->id()]));
    $this->assertEquals('Target fb', new TranslatableMarkup('Source', [], ['langcode' => $this->fallback->id()]));
    $this->assertEquals('Source', new TranslatableMarkup('Source', [], ['langcode' => $this->french->id()]));
  }

  /**
   * Test with an invalid fallback set.
   */
  public function testInvalidFallbackConfigured(): void {
    $this->dutch->setThirdPartySetting('missing_translation_fallback_language', 'langcode', 'invalid');
    $this->assertEquals('Target nl', new TranslatableMarkup('Source', [], ['langcode' => $this->dutch->id()]));
    $this->assertEquals('Target fb', new TranslatableMarkup('Source', [], ['langcode' => $this->fallback->id()]));
    $this->assertEquals('Source', new TranslatableMarkup('Source', [], ['langcode' => $this->french->id()]));
  }

}
