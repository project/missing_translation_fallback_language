# Missing Translation Fallback Language

The Missing Translation Fallback Language module allows you to set a fallback
language to use when translations are missing for a specific language.

For example, you have a website with two configured languages, Dutch and Flemish.
For some words there are specific Flemish translations, but for most of the strings
the Dutch translation will do. You can set the Dutch language as a fallback for
the Flemish translation.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/missing_translation_fallback_language).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/missing_translation_fallback_language).


## Installation and configuration
Install as you would normally install a contributed Drupal module. After that,
navigate to the /admin/config/regional/language page and optionally configure
a fallback language for each language.
