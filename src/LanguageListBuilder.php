<?php

namespace Drupal\missing_translation_fallback_language;

use Drupal\Core\Entity\EntityInterface;
use Drupal\language\LanguageListBuilder as DrupalLanguageListBuilder;

/**
 * Defines a class to build a listing of language entities.
 *
 * @see \Drupal\language\LanguageListBuilder
 */
class LanguageListBuilder extends DrupalLanguageListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = parent::buildHeader();

    $operations = $header['operations'];
    unset($header['operations']);
    $header['missing_translation_fallback_language'] = $this->t('Language for missing translations');
    $header['operations'] = $operations;
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /**
     * @var \Drupal\language\ConfigurableLanguageInterface $entity
     */
    $row = parent::buildRow($entity);

    $operations = $row['operations'];
    unset($row['operations']);

    $missing_translation_fallback_language_langcode = $entity->getThirdPartySetting('missing_translation_fallback_language', 'langcode');
    $missing_translation_fallback_language_langugage = !empty($missing_translation_fallback_language_langcode) ? $this->storage->load($missing_translation_fallback_language_langcode) : NULL;

    $row['missing_translation_fallback_language'] = [
      '#markup' => empty($missing_translation_fallback_language_langugage) ? $this->t('None') : $missing_translation_fallback_language_langugage->label(),
    ];
    $row['operations'] = $operations;

    return $row;
  }

}
