<?php

namespace Drupal\missing_translation_fallback_language;

use Drupal\language\ConfigurableLanguageInterface;
use Drupal\locale\LocaleTranslation as DrupalLocaleTranslation;

/**
 * String translator using the locale module which supports fallback.
 *
 * Full featured translation system using locale's string storage and
 * database caching.
 */
class LocaleTranslation extends DrupalLocaleTranslation {

  /**
   * {@inheritDoc}
   */
  public function getStringTranslation($langcode, $string, $context) {
    $fallback_cache = &drupal_static('missing_translation_fallback_language:fallback', []);
    $translated_string = parent::getStringTranslation($langcode, $string, $context);

    if ($translated_string === FALSE) {
      // No translation available, use the fallback if available.
      if (!is_array($fallback_cache) || !isset($fallback_cache[$langcode])) {
        // Get the fallback. Ignore complaints about dependency injection
        // because it would create a circular dependency error.
        // phpcs:ignore
        $configurable_language = \Drupal::entityTypeManager()
          ->getStorage('configurable_language')
          ->load($langcode);

        $fallback_cache[$langcode] = $configurable_language instanceof ConfigurableLanguageInterface ? $configurable_language->getThirdPartySetting('missing_translation_fallback_language', 'langcode') : '';
      }

      if (empty($fallback_cache[$langcode])) {
        // No fallback configured.
        return FALSE;
      }

      // Return the translation from the fallback language.
      return parent::getStringTranslation($fallback_cache[$langcode], $string, $context);
    }

    return $translated_string;
  }

}
